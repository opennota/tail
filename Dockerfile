FROM golang

RUN mkdir -p $GOPATH/src/gitlab.com/opennota/tail/
ADD . $GOPATH/src/gitlab.com/opennota/tail/

# expecting to fetch dependencies successfully.
RUN go get -v gitlab.com/opennota/tail

# expecting to run the test successfully.
RUN go test -v gitlab.com/opennota/tail

# expecting to install successfully
RUN go install -v gitlab.com/opennota/tail
RUN go install -v gitlab.com/opennota/tail/cmd/gotail

RUN $GOPATH/bin/gotail -h || true

ENV PATH $GOPATH/bin:$PATH
CMD ["gotail"]
