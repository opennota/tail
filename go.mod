module gitlab.com/opennota/tail

go 1.18

require (
	github.com/fsnotify/fsnotify v1.5.4
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7
)

require golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
